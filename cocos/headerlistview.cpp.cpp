#include "HeaderListView.h"

USING_NS_CC;


HeaderListView* HeaderListView::create(cocos2d::Node* header, float minHeaderHeight)
{
	HeaderListView* newInstance = new (std::nothrow) HeaderListView;
	if (newInstance && newInstance->init(header, minHeaderHeight))
	{
		newInstance->autorelease();
		return newInstance;
	}

	CC_SAFE_DELETE(newInstance);

	return nullptr;
}


bool HeaderListView::init(cocos2d::Node* header, float minHeaderHeight)
{
	if (!ui::ListView::init())
	{
		return false;
	}
	_headerStencil = DrawNode::create();
	_headerClipper = ClippingNode::create(_headerStencil);
	Node::addChild(_headerClipper, 42, "");

	_minHeaderHeight = minHeaderHeight;

	_header = header;
	_header->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_headerClipper->addChild(_header);

	auto headerItem = ui::Widget::create();
	headerItem->ignoreContentAdaptWithSize(false);
	headerItem->setContentSize(header->getContentSize());
	ui::ListView::insertCustomItem(headerItem, 0);

	return true;
}


HeaderListView::~HeaderListView()
{

}


void HeaderListView::insertCustomItem(cocos2d::ui::Widget* item, ssize_t index)
{
	ui::ListView::insertCustomItem(item, index + 1);
}



void HeaderListView::removeAllItems()
{
	while (_items.size() > 1)
	{
		this->removeLastItem();
	}

	ui::ListView::removeAllItems();
}


cocos2d::ui::Widget* HeaderListView::getItem(ssize_t index) const
{
	return ui::ListView::getItem(index + 1);
}


ssize_t HeaderListView::getIndex(cocos2d::ui::Widget* item) const
{
	return ui::ListView::getIndex(item) - 1;
}


cocos2d::Vector<cocos2d::ui::Widget*>& HeaderListView::getItems()
{
//MARK: THIS FUNCTION IS NOT SUPPORTED
	throw std::bad_function_call();
}


void HeaderListView::removeAllChildren()
{
	ui::ListView::removeAllChildren();
}


void HeaderListView::setHeaderRelativeOffsetCallback(std::function<void(float)> callback)
{
	_headerOffsetChanged = callback;
}


void HeaderListView::update(float dt)
{
	ui::ScrollView::update(dt);

	if (_header != nullptr && !_items.empty())
	{
		auto headerItem             = _items.at(0);
		Vec2 headerItemBottomPos    = this->convertToNodeSpace(headerItem->convertToWorldSpace(Vec2::ZERO));
		Vec2 clipperPos             = {0, std::min(this->getContentSize().height - _minHeaderHeight,
				std::max(this->getContentSize().height - _header->getContentSize().height, headerItemBottomPos.y))};
		float headerHeight          = this->getContentSize().height - clipperPos.y;

		_headerClipper->setPosition(clipperPos);
		_headerStencil->clear();
		_headerStencil->drawSolidRect(Vec2::ZERO, {this->getContentSize().width, headerHeight}, Color4F::WHITE);

		if (_headerOffsetChanged)
		{
			float relativeOffset = (headerHeight - _minHeaderHeight) / (_header->getContentSize().height - _minHeaderHeight);
			_headerOffsetChanged(relativeOffset);
		}
	}
}


Vector<Node*>& HeaderListView::getChildren()
{
	return _children;
}


const cocos2d::Vector<Node*>& HeaderListView::getChildren() const
{
	return _children;
}